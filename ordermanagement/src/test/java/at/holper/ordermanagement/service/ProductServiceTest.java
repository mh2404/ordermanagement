package at.holper.ordermanagement.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import at.holper.ordermanagement.IProductDAO;
import at.holper.ordermanagement.domain.Product;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductServiceTest {
	@Autowired
	private IProductDAO productDao;

	@Autowired
	ProductServiceImpl productService;

	@Test
	public void shouldReturnCreatedProduct() {
		LocalDateTime creationDateTime = LocalDateTime.of(2019, 11, 17, 12, 15, 00);
		Product newProduct = createNewProduct(creationDateTime);
		productDao.save(newProduct);

		Product productPostSaved = productService.getById(newProduct.getId());

		assertNotNull(productPostSaved, "Product shouldn't be null");
		assertThat(productPostSaved.getName(), equalTo("new Productname"));
		assertThat(productPostSaved.getPrice(), equalTo(new BigDecimal("100.00")));
		assertThat(productPostSaved.getCreationDate(), equalTo(creationDateTime));
		assertThat(productPostSaved.getLastPriceChangeTime(), equalTo(creationDateTime));
	}

	@Test
	public void shouldReturnNullForNotExistingProduct() {
		Product product = productService.getById(123L);

		assertNull(product);
	}

	@Test
	public void shouldUpdateProductWithPriceChangedDateUnequalCreationDateTime() {
		LocalDateTime creationDateTime = LocalDateTime.of(2019, 11, 17, 13, 00, 00);
		Product newProduct = createNewProduct(creationDateTime);
		productDao.save(newProduct);

		Product productPostSaved = productService.getById(newProduct.getId());

		productPostSaved.setName("existing Productname");
		productPostSaved.setPrice(new BigDecimal("200.00"));
		productService.update(productPostSaved);

		Product productPostUpdated = productService.getById(newProduct.getId());

		assertNotNull(productPostUpdated, "Product shouldn't be null");
		assertThat(productPostUpdated.getName(), equalTo("existing Productname"));
		assertThat(productPostUpdated.getPrice(), equalTo(new BigDecimal("200.00")));
		assertThat(productPostUpdated.getCreationDate(), equalTo(creationDateTime));
		assertThat(productPostUpdated.getLastPriceChangeTime(), not(creationDateTime));
	}

	@Test
	public void shouldNotUpdateProductWithPriceChangedDateUnequalCreationDateTime() {
		LocalDateTime creationDateTime = LocalDateTime.of(2019, 11, 17, 13, 00, 00);
		Product newProduct = createNewProduct(creationDateTime);
		productDao.save(newProduct);

		Product productPostSaved = productService.getById(newProduct.getId());

		productPostSaved.setName("existing Productname");
		productPostSaved.setPrice(new BigDecimal("100.00"));
		productService.update(productPostSaved);

		Product productPostUpdated = productService.getById(newProduct.getId());

		assertNotNull(productPostUpdated, "Product shouldn't be null");
		assertThat(productPostUpdated.getName(), equalTo("existing Productname"));
		assertThat(productPostUpdated.getPrice(), equalTo(new BigDecimal("100.00")));
		assertThat(productPostUpdated.getCreationDate(), equalTo(creationDateTime));
		assertThat(productPostUpdated.getLastPriceChangeTime(), equalTo(creationDateTime));
	}

	private Product createNewProduct(LocalDateTime creationDateTime) {
		Product product = new Product();
		product.setName("new Productname");
		product.setPrice(new BigDecimal("100.00"));
		product.setCreationDate(creationDateTime);
		product.setLastPriceChangeTime(product.getCreationDate());
		return product;
	}

}
