package at.holper.ordermanagement.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import at.holper.ordermanagement.IOrderDAO;
import at.holper.ordermanagement.domain.Ordering;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class OrderServiceTest {
	@Autowired
	private IOrderDAO orderDao;

	@Autowired
	OrderServiceImpl orderService;

	@Test
	public void shouldReturnCreatedOrdering() {
		LocalDateTime creationDateTime = LocalDateTime.of(2019, 11, 17, 12, 15, 00);
		Ordering newOrdering = createNewOrdering(creationDateTime);
		orderDao.save(newOrdering);

		Ordering productPostSaved = orderService.getById(newOrdering.getId());

		assertNotNull(productPostSaved, "Ordering shouldn't be null");
		assertThat(productPostSaved.getCustomerEmail(), equalTo("newCustomer@test.com"));
		assertThat(productPostSaved.getCreationDate(), equalTo(creationDateTime));
	}

	@Test
	public void shouldReturnNullForNotExistingOrdering() {
		Ordering product = orderService.getById(123L);

		assertNull(product);
	}

	private Ordering createNewOrdering(LocalDateTime creationDateTime) {
		Ordering product = new Ordering();
		product.setCustomerEmail("newCustomer@test.com");
		product.setCreationDate(creationDateTime);
		return product;
	}

}
