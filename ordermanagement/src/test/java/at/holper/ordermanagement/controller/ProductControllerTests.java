package at.holper.ordermanagement.controller;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import at.holper.ordermanagement.IProductDAO;
import at.holper.ordermanagement.domain.Product;
import at.holper.ordermanagement.service.IProductService;

@WebMvcTest(ProductController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class ProductControllerTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private IProductService service;

	@MockBean
	private IProductDAO dao;

	@Test
	public void shouldlistProducts() throws Exception {
		this.mvc.perform(get("/products").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andDo(document("list-products"));
	}

	@Test
	public void shouldAddProduct() throws Exception {

		// given
		String productBody = "{\"name\":\"Test product\", \"price\":\"105.00\"}";
		Product newProduct = createProduct("Test content", "105.00");

		// when
		when(service.create(newProduct)).thenReturn(1L);

		// then
		mvc.perform(
				post("/products").content(productBody).contentType(APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andDo(document("create-product"));

	}

	@Test
	public void shouldUpdateProduct() throws Exception {

		// given
		String productBody = "{\"id\":\"1\", \"name\":\"Test product\", \"price\":\"200.00\"}";
		Product existingProduct = createProduct("Test content", "105.00");
		existingProduct.setId(1L);

		// when
		when(service.getById(1L)).thenReturn(existingProduct);

		// then
		mvc.perform(put("/products/1").content(productBody).contentType(APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(document("update-product"));

	}

	private Product createProduct(String name, String price) {
		Product newProduct = new Product();
		newProduct.setName(name);
		newProduct.setPrice(new BigDecimal(price));
		return newProduct;
	}

}
