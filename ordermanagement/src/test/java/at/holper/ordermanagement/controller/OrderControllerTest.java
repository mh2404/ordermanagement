package at.holper.ordermanagement.controller;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import at.holper.ordermanagement.IOrderDAO;
import at.holper.ordermanagement.domain.Ordering;
import at.holper.ordermanagement.service.IOrderService;

@WebMvcTest(OrderController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class OrderControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private IOrderService service;

	@MockBean
	private IOrderDAO dao;

	@Test
	public void shouldlistOrderingBetween() throws Exception {
		this.mvc.perform(get("/orders/2019-11-19/2019-11-20").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andDo(document("list-orders-between"));
	}

	@Test
	public void shouldAddOrdering() throws Exception {

		// given
		String productBody = "{\"customerEmail\":\"newcustomer@test.com\"}";
		Ordering newOrder = createOrder();

		// when
		when(service.create(newOrder)).thenReturn(1L);

		// then
		mvc.perform(post("/orders").content(productBody).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(document("create-order"))
				.andDo(print());

	}

	private Ordering createOrder() {
		Ordering newOrder = new Ordering();
		newOrder.setCustomerEmail("newcustomer@test.com");
		newOrder.setCreationDate(LocalDateTime.now());
		return newOrder;
	}

}
