package at.holper.ordermanagement.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Each order should be recorded
 * 
 * @author martin.holper
 *
 */
@Getter
@Setter
@Entity
public class Ordering {

	@NotNull
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * It should also have the buyer’s e-mail,
	 */
	@Email
	private String customerEmail;

	/**
	 * and thetime the order was placed.
	 */
	@NotNull
	private LocalDateTime creationDate;

	/**
	 * ..and have a list of products.
	 */
	@OneToMany(mappedBy = "ordering")
	Set<OrderingProducts> orderProducts = new HashSet<>();

	public Set<OrderingProducts> getOrderProducts() {
		return orderProducts;
	}

	public void addProductToOrder(Product product) {
		OrderingProducts orderProducts = new OrderingProducts();
		orderProducts.setProduct(product);
		orderProducts.setCreatedAt(LocalDateTime.now());
		orderProducts.setOrdering(this);
		orderProducts.setProductPriceAtOrder(product.getPrice());
	}

	public void removeProductFromOrder(Product product) {
		Optional<OrderingProducts> findAny = orderProducts.stream().filter(Objects::nonNull)
				.filter(prod -> product.getId().equals(prod.getProduct().getId())).findAny();
		if (findAny.isPresent()) {
			orderProducts.remove(findAny.get());
		}
	}

	/**
	 * The total value of the order should always be calculated, based on the prices
	 * of the products in it.
	 * 
	 */
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	@Transient
	private BigDecimal orderPrice;

	public BigDecimal getOrderPrice() {
		BigDecimal calculatedOrderPrice = BigDecimal.ZERO;
		if (this.getOrderProducts().size() > 0) {
			calculatedOrderPrice = this.getOrderProducts().stream().map(OrderingProducts::getProductPriceAtOrder)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
		}
		return calculatedOrderPrice;
	}

}
