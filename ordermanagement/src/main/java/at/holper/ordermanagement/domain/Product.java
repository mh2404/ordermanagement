package at.holper.ordermanagement.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * A product should have a name and some representation of its price.
 * 
 * @author martin.holper
 *
 */
@Getter
@Setter
@Entity
public class Product {

	@NotNull
	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private String name;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private BigDecimal price;

	private LocalDateTime creationDate;

	/**
	 * It should be possible to change the product’s price, but this shouldn’t
	 * affect the total value of orders which have already been placed.
	 */
	private LocalDateTime lastPriceChangeTime;

}
