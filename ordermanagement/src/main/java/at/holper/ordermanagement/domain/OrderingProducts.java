package at.holper.ordermanagement.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class OrderingProducts {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn(name = "order_id")
	Ordering ordering;

	@ManyToOne
	@JoinColumn(name = "product_id")
	Product product;

	LocalDateTime createdAt;

	BigDecimal productPriceAtOrder;

}