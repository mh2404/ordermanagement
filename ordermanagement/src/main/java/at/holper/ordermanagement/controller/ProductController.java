package at.holper.ordermanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;

import at.holper.ordermanagement.domain.Product;
import at.holper.ordermanagement.service.IProductService;
import at.holper.ordermanagement.util.RestPreconditions;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private IProductService service;

	@GetMapping
	public List<Product> findAll() {
		return service.findAll();
	}

	@GetMapping(value = "/{id}")
	public Product findById(@PathVariable("id") Long id) {
		return RestPreconditions.checkFound(service.getById(id));
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(@RequestBody Product resource) {
		Preconditions.checkNotNull(resource);
		return service.create(resource);
	}

	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable("id") Long id, @RequestBody Product resource) {
		Preconditions.checkNotNull(resource);
		Preconditions.checkNotNull(service.getById(resource.getId()));
		service.update(resource);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") Long id) {
		service.deleteById(id);
	}

}
