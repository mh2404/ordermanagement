package at.holper.ordermanagement.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;

import at.holper.ordermanagement.domain.Ordering;
import at.holper.ordermanagement.service.IOrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private IOrderService service;

	@GetMapping(value = "/{from}/{to}")
	public List<Ordering> findAllOrdersBetween(@PathVariable("from") @DateTimeFormat(iso = ISO.DATE) LocalDate from,
			@PathVariable("to") @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
		return service.findAllInPeriod(from.atStartOfDay(), to.atStartOfDay());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(@RequestBody Ordering resource) {
		Preconditions.checkNotNull(resource);
		return service.create(resource);
	}

}
