package at.holper.ordermanagement;

import org.springframework.data.jpa.repository.JpaRepository;

import at.holper.ordermanagement.domain.Product;

public interface IProductDAO extends JpaRepository<Product, Long> {

	Product findByName(String name);

}
