package at.holper.ordermanagement;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.holper.ordermanagement.domain.Ordering;

public interface IOrderDAO extends JpaRepository<Ordering, Long> {

	List<Ordering> findAllByCreationDateBetween(LocalDateTime from, LocalDateTime to);
}
