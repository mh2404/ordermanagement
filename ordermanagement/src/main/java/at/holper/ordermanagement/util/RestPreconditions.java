package at.holper.ordermanagement.util;

import at.holper.ordermanagement.exception.MyResourceNotFoundException;

/**
 * Guava style RestPreconditions utility.
 * 
 * @author martin.holper
 *
 */
public class RestPreconditions {
	public static <T> T checkFound(T resource) {
		if (resource == null) {
			throw new MyResourceNotFoundException();
		}
		return resource;
	}
}
