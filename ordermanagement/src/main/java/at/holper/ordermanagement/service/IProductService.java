package at.holper.ordermanagement.service;

import java.util.List;

import at.holper.ordermanagement.domain.Ordering;
import at.holper.ordermanagement.domain.Product;

public interface IProductService {
	List<Product> findAll();

	Product getById(Long id);

	Long create(Product product);

	void update(Product product);

	void deleteById(Long id);

	void addProductToOrder(Product product, Ordering order);
}
