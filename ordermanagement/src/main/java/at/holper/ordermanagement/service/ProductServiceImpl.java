package at.holper.ordermanagement.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import at.holper.ordermanagement.IProductDAO;
import at.holper.ordermanagement.domain.Ordering;
import at.holper.ordermanagement.domain.Product;

@Service
public class ProductServiceImpl implements IProductService {

	private final IProductDAO productDao;

	public ProductServiceImpl(IProductDAO productDao) {
		this.productDao = productDao;
	}

	@Override
	public List<Product> findAll() {
		return productDao.findAll();
	}

	@Override
	public Product getById(Long id) {
		return productDao.findById(id).orElse(null);

	}

	@Override
	public Long create(Product product) {
		return productDao.saveAndFlush(product).getId();
	}

	@Override
	public void update(Product product) {
		// update pricetimestamp if price has changed!
		if (getById(product.getId()).getPrice().compareTo(product.getPrice()) != 0) {
			product.setLastPriceChangeTime(LocalDateTime.now());
		}
		productDao.save(product);
	}

	@Override
	public void deleteById(Long id) {
		productDao.deleteById(id);
	}

	@Override
	public void addProductToOrder(Product product, Ordering order) {
		if (Objects.isNull(order)) {
			throw new IllegalArgumentException("Order must not be null");

		}
		order.addProductToOrder(product);
	}

}
