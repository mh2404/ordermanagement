package at.holper.ordermanagement.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import at.holper.ordermanagement.IOrderDAO;
import at.holper.ordermanagement.domain.Ordering;

@Service
public class OrderServiceImpl implements IOrderService {

	private final IOrderDAO orderDao;

	public OrderServiceImpl(IOrderDAO orderDAO) {
		this.orderDao = orderDAO;
	}

	@Override
	public Ordering getById(Long id) {
		return orderDao.findById(id).orElse(null);

	}

	@Override
	public Long create(Ordering order) {
		return orderDao.saveAndFlush(order).getId();
	}

	@Override
	public List<Ordering> findAllInPeriod(LocalDateTime from, LocalDateTime to) {
		return orderDao.findAllByCreationDateBetween(from, to);
	}

}
