package at.holper.ordermanagement.service;

import java.time.LocalDateTime;
import java.util.List;

import at.holper.ordermanagement.domain.Ordering;

public interface IOrderService {

	Long create(Ordering order);

	List<Ordering> findAllInPeriod(LocalDateTime periodBegin, LocalDateTime periodEnd);

	Ordering getById(Long id);

}
